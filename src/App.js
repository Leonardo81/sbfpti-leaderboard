import React from 'react';
import './App.css';
import LeaderBoard from './components/leaderboard';
import Nyoba from './components/nyoba';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';


function App() {
  const routes = [
    {
      component: LeaderBoard,
      exact: true,
      path: "/"
    },
    {
      component: Nyoba,
      exact: true,
      path: "/nyoba"
    }
  ];
  const pages = routes.map(route => (
    <Route
      component={route.component}
      exact={route.exact}
      path={route.path}
    />
  ));

  return (
    <div className="App">
      <Router>
        <Switch>
          {pages}
        </Switch>
      </Router>
    </div>
  );
}

export default App;
