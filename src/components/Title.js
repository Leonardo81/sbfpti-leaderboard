import React, {Component} from 'react';
import zootopia from '../zootopia.png';
import '../App.css';

export default class Title extends Component {
  render(){
      return (
            <div className="title">
                <img src={zootopia} alt="zootopia"/>
                <h1>
                    SBF PTI LeaderBoard
                </h1>
            </div>
        );
    }
}