import React from 'react';
import '../App.css';
import Title from './Title';
import * as firebase from "firebase/app";
import "firebase/firestore";

export default class LeaderBoard extends React.Component {
    constructor(){
        super()
        this.state = {
            firebaseConfig: 
            {
                apiKey: "AIzaSyD8cG3z15MM-cm27Fh1nT5E2Vy3tvyT_ok",
                authDomain: "sbf-leaderboard.firebaseapp.com",
                databaseURL: "https://sbf-leaderboard.firebaseio.com",
                projectId: "sbf-leaderboard",
                storageBucket: "sbf-leaderboard.appspot.com",
                messagingSenderId: "839343508297",
                appId: "1:839343508297:web:36480797dfb7430b11a898"
            }
        };
    }
    
    componentDidMount(){
        firebase.initializeApp(this.state.firebaseConfig);
        const database = firebase.firestore();
        database.collection('tests').orderBy('score').get()
            .then(response => {
                this.setState({scores: response.docs.map(doc=>doc.data())})
            })
    }
    grade(index){
        if(index===0){ return "gold"; }
        else if(index===1){ return "silver"; }
        else if(index===2){ return "bronze"; }
        return "";     
    }

  render(){
      const { scores } = this.state;
      return (
            <div className="App-header">
                <Title />
                    {scores && scores.reverse().map((x,i) => {
                        return(
                            <div className={`item ${this.grade(i)}`}>
                                <p>{i+1}.</p>
                                <p className="nama">{x.nama}</p>
                                <p className="point">{x.score} Pts</p>
                            </div>
                        ); 
                    })}
                    { !scores && "Fetching data...."}
            </div>

        );
    }
}