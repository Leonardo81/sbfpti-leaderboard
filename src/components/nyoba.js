import React from 'react';
import '../App.css';
import * as firebase from "firebase/app";
import "firebase/firestore";

export default class Nyoba extends React.Component {
    constructor(){
        super()
        this.state = {
            firebaseConfig: 
            {
                apiKey: "AIzaSyD8cG3z15MM-cm27Fh1nT5E2Vy3tvyT_ok",
                authDomain: "sbf-leaderboard.firebaseapp.com",
                databaseURL: "https://sbf-leaderboard.firebaseio.com",
                projectId: "sbf-leaderboard",
                storageBucket: "sbf-leaderboard.appspot.com",
                messagingSenderId: "839343508297",
                appId: "1:839343508297:web:36480797dfb7430b11a898"
            }
        };
        this.sendData = this.sendData.bind(this);
    }
    
    componentDidMount(){
        firebase.initializeApp(this.state.firebaseConfig);
        const db = firebase.firestore();
        db.collection('nyoba').get()
        .then(response => {
            this.setState({db:db, data: response.docs.map(doc=>doc.data())})
        })
    }

    sendData(e){
        if(e.keyCode === 13 && e.currentTarget.value !== ""){
            alert(e.currentTarget.value);
            this.state.db.collection('nyoba').add({
                nama: e.currentTarget.value
            })
            e.currentTarget.value = "";
        }
    }
  render(){
      return (
          <div>
            <div className="App-header">
                halo { this.state.data && this.state.data.map(a=> {return a.nama}) }
            </div>
            <div>
                <input type="text" onKeyDown={this.sendData} />
            </div>
          </div>

        );
    }
}